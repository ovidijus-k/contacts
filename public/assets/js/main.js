'use strict';

var $ = jQuery;

var global = (function () {

    function init()
    {
        $('[data-action-delete]').click(this.deleteEntity);
        $('[data-action-toggle-check-all]').change(this.toggleCheckAll);
        $('[data-action-delete-checked]').click(this.deleteCheckedEntities);
    }


    function deleteEntity(e)
    {

        if ( !confirm('Are you sure?') )
            return;

        var el = $(this);
        e.preventDefault();

        $.ajax({
            url: params.baseUrl + 'contacts/' + $(this).data('action-delete') + '/delete',
            dataType: 'json',
            type: 'DELETE',
            success: function(data) {
                alert(data.message);
                el.parents('tr').remove();
            }
        })
    }


    function deleteCheckedEntities(e)
    {
        e.preventDefault();

        if ( !confirm('Are you sure?') )
            return;

        var ids = '';

        $('input[name="entities[]"]:checked').each(function(index, el) {
            ids += el.value + ',';
        });

        ids = ids.replace(/(^,)|(,$)/g, "");

        if (ids == '') {
            alert('Nothing selected');
            return;
        }

        $.ajax({
            url: params.baseUrl + 'contacts/' + ids + '/delete',
            dataType: 'json',
            type: 'DELETE',

            success: function(data) {
                alert(data.message);
            }
        })

    }


    function toggleCheckAll(e)
    {
        var state = $(this).prop('checked');
        $(this).parents('table').find('tbody input[type="checkbox"]').prop('checked', state);
    }

    return {
        init: init,
        deleteEntity: deleteEntity,
        deleteCheckedEntities: deleteCheckedEntities,
        toggleCheckAll : toggleCheckAll,
    }

})();

global.init();
