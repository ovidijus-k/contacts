<?php
namespace App\Core;

define('APP_PATH', dirname(__DIR__) . '/App');

spl_autoload_register(function ($class) {
	$root = dirname(__DIR__);
	$file = $root . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';

	if (is_readable($file)) {
		require $file;
	}
});

$router = Router::getInstance();

// $router->add('{controller}/{action}');
// $router->add('{controller}/{id:\d+}/{action}');
// $router->add('', ['controller' => 'Contacts', 'action' => 'index']);

$app = new App();

// Generate contacts
// $generator = new \App\Generators\ContactsGenerator();
// $generator->generateContacts();


$app->run();
