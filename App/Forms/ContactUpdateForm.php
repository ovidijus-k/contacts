<?php
namespace App\Forms;
use App\Core\Request as Request;
use App\Core\Router as Router;

class ContactUpdateForm extends \App\Forms\ContactForm
{

	function __construct()
	{
		parent::__construct('contacts_update');
	}


	protected function saveData()
    {

        $router = Router::getInstance();
        $id = $router->getParam('id');
        if (empty($id)) {
            throw new \Exception('Id not provided');
        }

    	$contact = new \App\Models\Contact();

    	$data = [
    		'fullname' => $this->request['fullname'],
    		'phone' => $this->request['phone'],
    	];

    	$contact->update($id, $data);

        return true;
    }
}
