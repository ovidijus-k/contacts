<?php
namespace App\Forms;

class ContactForm extends \App\Core\Form
{
	function __construct($formId)
	{
		$fields = [
			'fullname' => [
				'rules' => [
					'required',
					'name',
					'minChars' => 4,
				]
			],
			'phone' => [
				'rules' => [
					'required',
					'phone',
					'minChars' => 9,
					'maxChars' => 12,
				],
			],
		];

		parent::__construct($formId, $fields);
	}


	protected function sanitizeData($data)
	{
		$satinized = [];

		$sanitized['phone'] = isset($data['phone']) ? trim(htmlspecialchars($data['phone'])) : '';
		$sanitized['fullname'] = isset($data['fullname']) ? trim(htmlspecialchars($data['fullname'])) : '';

		return $sanitized;
	}

}