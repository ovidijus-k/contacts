<?php
namespace App\Forms;

class ContactCreateForm extends \App\Forms\ContactForm
{

	function __construct()
	{
		parent::__construct('contacts_create');
	}
	
    protected function afterValidate()
    {
 		// Additional validation: do not allow already existing fullname or email

 		$contact = new \App\Models\Contact();
 		if ($contact->exists('fullname', $this->request['fullname'])) {
			$this->validator->addMessage('fullname', 'Already exists');
			$this->validator->invalidate();
			return false;
 		} else if ($contact->exists('phone', $this->request['phone'])) {
 			$this->validator->addMessage('phone', 'Already exists');
 			$this->validator->invalidate();
 			return false;
 		}

 		return true;

    }


	protected function saveData()
    {

    	$contact = new \App\Models\Contact();

    	$data = [
    		'fullname' => $this->request['fullname'],
    		'phone' => $this->request['phone'],
    	];

    	$contact->create($data);

        return true;
    }
}