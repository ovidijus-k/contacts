<?php
namespace App\Controllers;


use App\Forms\ContactCreateForm as ContactCreateForm;
use App\Forms\ContactUpdateForm as ContactUpdateForm;
use App\Core\Request as Request;
use App\Core\View as View;
use App\Models\Contact as Contact;


class ContactsController extends \App\Core\Controller
{


	public function indexAction()
	{
        // TODO: remove
        // $g = new \App\Generators\ContactsGenerator();
        // $g->generateContacts(100);

		// TODO: remove example
		//		$args = [
		//			'page' => $page,
		//			'where' => [
		//				'relation' => 'AND',
		//				[
		//					'field' => 'fullname',
		//					'compare' => 'LIKE',
		//					'value' => '%Varga%',
		//				],
		//				[
		//					'relation' => 'OR',
		//					[
		//						'field' => 'phone',
		//						'compare' => 'LIKE',
		//						'value' => '%123%',
		//					],
		//					[
		//						'field' => 'phone',
		//						'compare' => 'LIKE',
		//						'value' => '%456%',
		//					]
		//				]
		//			]
		//		];


		// Query

        $args = [
        	'pagesize' => 10,
            'page' => $this->getParam('page', 1),
        ];

        $searchParams = $this->request->get('search');
        if (is_array($searchParams) && !empty($searchParams)) {
			$args['where']['relation'] = 'AND';
        	foreach ($searchParams as $key => $val) {
        		$args['where'][] = [
        			'field' => $key,
        			'value' => '%' . $val . '%',
        			'compare' => 'LIKE',
        		];
        	}

        }

		$model = new Contact();

		$model->setArgs($args);

		$contacts = $model->query();


		// Paginator

		$params = [
			'baseUri' => 'contacts',
		];

		$paginator = new \App\Core\Paginator($model, $params);


		// Render contact list

		return View::render('index', [
			'contacts' => $contacts,
			'pages' => $paginator->getPages(),
		]);
	}


	public function createAction()
	{

		return View::render('contacts/create', ['fullname' => '', 'phone' => '']);
	}


	public function editAction()
	{
        $contact = new Contact();
        $contact = $contact->get($this->getParam('id'));
        return View::render('contacts/edit',
            [
                'contact' => $contact,
            ]);
	}


	public function updateAction()
	{
        $form = new ContactUpdateForm();
		$form->process();
		$validator = $form->getValidator();

        $contact = new Contact();
        $contact = $contact->get($this->getParam('id'));
        return View::render('contacts/edit',
			[
                'contact' => $contact,
				'errors' => $validator->getMessages(),
				'statusCode' => $validator->getStatusCode(),
				'statusMessage' => $validator->getStatusMessage()
			]);
	}


	public function storeAction()
	{
		$form = new ContactCreateForm();
		$form->process();
		$validator = $form->getValidator();

		return View::render('contacts/create',
			[
				'errors' => $validator->getMessages(),
				'statusCode' => $validator->getStatusCode(),
				'statusMessage' => $validator->getStatusMessage()
			]);
	}


	public function deleteAction()
	{
        $id = $this->getParam('id');
        if (empty($id)) {
            throw new \Exception('Id not provided');
        }

        $contact = new Contact();

        if ($contact->delete($id)) {
            $response = [
                'status' => 'success',
                'message' => 'Deleted',
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Failed to delete',
            ];
        }

        echo json_encode($response);
        die();
	}

}
