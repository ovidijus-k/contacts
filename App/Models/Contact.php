<?php
namespace App\Models;

use PDO;

class Contact extends \App\Core\Model
{

	// Database table (required)
	protected $table = 'contacts';

	// Override default query parameters
	protected $queryArgs = [
		'pagesize' => 25,
	];
	
}