<?php

namespace App\Generators;

use App\Models\Contact as Contact;

class ContactsGenerator
{

	private $data = [
		'firstnames' => [
			'Adolf',
			'Anna',
			'Kayleigh',
			'Elisa',
			'Major',
			'Alan',
			'Abdulmajid',
			'Jean',
			'Joseph',
			'Knut',
			'Claire',
			'Tom',
			'Michael',
			'Albert',
			'Olivia',
			'Bela',
			'Baby',
			'Adult',
			'David',
			'Deborah',
            'Kim',
			'Kurt',
            'Frodo',
            'Gendalf',
            'Dierdre',
            'Habib',
		],
		'lastnames' => [
			'Hilter',
			'Salin',
			'Jones',
			'Williams',
			'Ottoman',
			'Jarre',
			'Vonnegut',
			'Elrond',
			'Bartok',
			'Block',
			'Crusher',
			'Laser',
			'Kawasaki',
			'Byrne',
			'Genovaite',
			'Thermometer',
			'Strangelove',
            'Kubrick',
            'Tolkien',
            'Anthrax',
            'Bush',
            'Sensei',
		],
	];


	public function generateContacts($amount = 100)
	{

		for ($i = 1; $i <= $amount; $i++) {
			$fields = [
				'fullname' => $this->randomName(),
				'phone' => $this->randomPhone(),
			];

			$model = new Contact();
			$model->create($fields);
		}
	}

	public function randomName()
	{
		$firstnameIdx = array_rand($this->data['firstnames']);
		$lastnameIdx = $firstname = array_rand($this->data['lastnames']);
		return $this->data['firstnames'][$firstnameIdx] . ' ' . $this->data['lastnames'][$lastnameIdx];
	}

	public function randomPhone()
	{
		$phone = '+3706';
		for ($i = 0; $i < 7; $i++) {
			$phone .= rand(0, 9);
		}

		return $phone;
	}
}
