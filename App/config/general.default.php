<?php

return [

	// Url to public folder, should be document root
	'base-url' => 'http://website.com',

	// In case document root is not pointing to public folder, specify relative url to public folder
	// Ensure to use htaccess.public htaccess file
	'base-url-prefix' => false,
];
