<?php

return [
	'{controller}/{action}',
	'{controller}/{id:\d+}/{action}',
	[
		'pattern' => '{controller}/create',
		'method' => 'post',
		'params' => [
			'action' => 'store',
		]
	],
	[
		'pattern' => '{controller}/{id:\d+}/edit',
		'method' => 'post',
		'params' => [
			'action' => 'update',
		]
	],
    [
		'pattern' => '{controller}/{id:[0-9,]+}/delete',
		'method' => 'delete',
		'params' => [
			'action' => 'delete',
		]
	],
	[
		'pattern' => '{controller}/page/{page:\d+}',
		'params' => [
			'action' => 'index'
		]
	],
	[
		'pattern' => '',
		'params' => [
			'controller' => 'Contacts',
			'action' => 'index'
		]
	]
];
