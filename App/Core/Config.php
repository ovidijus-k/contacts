<?php
namespace App\Core;


class Config
{

	private static $config;


	private static function load()
	{

		// TODO: load from config/* files

		self::$config['db'] = require APP_PATH . '/config/db.php';

		self::$config['routes'] = require APP_PATH . '/config/routes.php';

		// General

		$general = require APP_PATH . '/config/general.php';

		self::$config = array_merge(self::$config, $general);
	}


	public static function get($key)
	{
		if (empty(self::$config))
			self::load();

		$levels = explode('.', $key);

		if ( empty($levels) )
			return null;

		$value = isset( self::$config[$levels[0]] ) ? self::$config[$levels[0]] : null;
		unset($levels[0]);

		foreach ($levels as $level) {
			if ( ! isset($value[$level]) ) {
				$value = null;
				break;
			} else {
				$value = $value[$level];
			}
		}

		return $value;
	}
}