<?php
namespace App\Core;

class View
{

	static function render($view, $args = [], $headerless = false)
	{
		$file = '../App/views/' . $view . '.php';

		if (is_readable($file)) {

			if ( ! $headerless)
				self::renderHeader();

			extract($args);
			require $file;

			if ( ! $headerless)
				self::renderFooter();

		} else {
			echo 'View ' . $file . ' not found';
		}
	}


	static function renderHeader()
	{
		require dirname(dirname(__FILE__)) . '/views' . '/layout/header.php';
	}


	static function renderFooter()
	{
		require dirname(dirname(__FILE__)) . '/views' . '/layout/footer.php';
	}


	// Helper methods

	static function url($path = '')
	{

		Helper::url($path);
	}

    static function token()
    {
        echo Form::getToken();
    }

	static function input($key, $default = false)
	{
		$request = Request::getInstance();
		return $request->has($key) ? htmlspecialchars($request->get($key)) : $default;
	}

    static function currentUrl()
    {
        $request = Request::getInstance();
        return $request->getUrl();
    }
}
