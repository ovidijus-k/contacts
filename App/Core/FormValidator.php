<?php
namespace App\Core;

class FormValidator
{
	const STATUS_PASSED = 'passed';
	const STATUS_FAILED = 'failed';

    protected $defaultMsg = [
    	'required'  => 'Cannot be empty',
        'minChars'  => 'Must be atleast %s characters long',
        'maxChars'  => 'Cannot be no longer than %s characters',
        'min'       => 'Must be greater or equal to %s',
        'max'       => 'Cannot be greater than %s',        
        'phone'     => 'Must a valid phone',
        'name'      => 'Not a valid name, must contain only letters',        
        'failed'    => 'Errors are present in form',
        'passed'    => 'Form successfully submitted',
    ];

    private $fields;

    private $status = [
        'status' => 'passed',
        'statusMsg' => 'Errors are present in form',
        'msg' => '',
        'fields' => [],
    ];


    function __construct($fields)
    {
		$this->fields = $fields;    	
    }

    function validate($request)
    {
        $request = Request::getInstance();
        
        foreach ($this->fields as $field => $fieldRules) {

            $value = $request->get($field);;

            foreach ($fieldRules['rules'] as $rule => $ruleVal) {

                if (is_numeric($rule)) {
                    $rule = $ruleVal;
                }

                switch ($rule) {
                    case 'required' : 
                        if ( empty($value) ) {
                            $msg = $this->getRuleMessage($field, $rule);
                            $this->addMessage($field, $msg);

                        }
                        break;  
                    case 'minChars' : 
                        if ( ! empty($value) && strlen($value) < $ruleVal ) {
                            $msg = $this->getRuleMessage($field, $rule);
                            if ($msg) {
                                $msg = sprintf($msg, $ruleVal);
                                $this->addMessage($field, $msg);
                            }
                        }
                        break;
                    case 'maxChars' :
                        if ( ! empty($value) && strlen($value) > $ruleVal ) {
                            $msg = $this->getRuleMessage($field, $rule);
                            if ($msg) {
                                $msg = sprintf($msg, $ruleVal);
                                $this->addMessage($field, $msg);
                            }
                        }
                        break;
                    case 'min':
                        if ( ! (!empty($value) && $value >= $ruleVal) ) {
                            $msg = $this->getRuleMessage($field, $rule);
                            if ($msg) {
                                $msg = sprintf($msg, $ruleVal);
                                $this->addMessage($field, $msg);
                            }
                        }
                        break;
                    case 'max':
                        if ( ! (!empty($value) && $value <= $ruleVal) ) {
                            $msg = $this->getRuleMessage($field, $rule);
                            if ($msg) {
                                $msg = sprintf($msg, $ruleVal);
                                $this->addMessage($field, $msg);
                            }
                        }
                        break;
                    case 'name' : 
                        $match = [];
                        $matched = preg_match('/[A-z\s]+/', $value, $match);

                        if ( !empty($value) && ($matched != 1 || $match[0] != $value) ) {
                            $msg = $this->getRuleMessage($field, $rule);
                            $this->addMessage($field, $msg);
                        }
                        break;                        
                    case 'phone' : 
                        $value = str_replace(' ', '', $value);
                        $match = [];
                        $matched = preg_match('/[0-9\+\s]+/', $value, $match);

                        if ( !empty($value) && ($matched != 1 || $match[0] != $value) ) {
                            $msg = $this->getRuleMessage($field, $rule);
                            $this->addMessage($field, $msg);
                        }
                        break;
                }
            }

            if ( empty($this->status['fields']) ) {
                 $this->setStatus(self::STATUS_PASSED);
            } else {
                $this->invalidate();
            }

        }

        return;
    }


    function getStatus()
    {
        return $this->status;
    }


    function getStatusCode()
    {
    	return $this->status['status'];
    }


    function getStatusMsg()
    {
    	return $this->status['statusMsg'];	
    }


    function setStatus($status, $msg = false)
    {
        $msg = $msg ? $msg : $this->defaultMsg[$status];
        $this->status['status'] = $status;
        $this->status['statusMsg'] = $msg;
    }    

    function invalidate($msg = false)
    {
        $msg = $msg ? $msg : $this->defaultMsg[self::STATUS_FAILED];
        $this->setStatus(self::STATUS_FAILED, $msg);
    }


    function hasPassed()
    {
        return $this->status['status'] == self::STATUS_PASSED;
    }


    function addMessage($field, $msg)
    {
        $this->status['fields'][$field] = $msg;
    }


    function getMessages()
    {
        return $this->status['fields'];
    }

    function getMessage($field)
    {
        return isset($this->status[$field]) ? $this->status[$field] : false;
    }


    function getStatusMessage()
    {
        return $this->status['statusMsg'];
    }


    function getRuleMessage($field, $rule)
    {
        $defaultMsg = isset($this->defaultMsg[$rule]) ? $this->defaultMsg[$rule] : $rule;

        $msg = isset($this->fields[$field]['messages'][$rule]) ? $this->fields[$field]['messages'][$rule] : $defaultMsg;

        return $msg;
    }

}