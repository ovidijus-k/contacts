<?php
/*
 * Parse query as SQL
 */

namespace App\Core;

class SQLQueryParser
{
	private $table;

	function __construct($table)
	{
		if (empty($table)) {
			throw new \Exception("Table not specified");
		}

		$this->table = $table;
	}

	function parseArgs($args)
	{
		if (! isset($args['pagesize']) ) {
			$args['pagesize'] = 10;
		}

		return $args;
	}

	function parseQuery($args, $type = 'select')
	{
		$args = $this->parseArgs($args);

		$bindParams = [];

		$sql = "SELECT ";

		if ($type == 'select') {
			$sql .= "*";
		} else if ($type == 'count') {
			$sql .= "count(*)";
		} else {
			$sql .= "*";
		}

		$sql .= " FROM " . $this->table;

		if (!empty($args['where'])) {
			$whereSql = ' WHERE ' . $this->parseWhereClause($args['where']);
            $sql .= $whereSql;
		}

		if ( !empty($args['page']) && !empty($args['pagesize']) ) {

			$sql .= " LIMIT :limit OFFSET :offset";

			$bindParams['offset'] = $args['pagesize'] * ($args['page'] - 1);
			$bindParams['limit'] = $args['pagesize'];

		}

        return [
            'sql' => $sql,
            'params' => $bindParams,
        ];

	}

	function parseInsertQuery($args)
	{
		$sql = "INSERT INTO " . $this->table . " (";


		// Fields

		$fields = implode(',', array_keys($args['fields']));

		$sql .= $fields . ") VALUES (";

		// Values

		$values = implode(',', array_values($args['fields']));

		foreach (array_keys($args['fields']) as $field) {
			$sql .= ':' . $field . ',';
		}

		$sql = rtrim($sql, ',');

		$sql .=  ")";

		return $sql;

	}

    function parseUpdateQuery($args)
    {
        $sql = 'UPDATE ' . $this->table . ' SET ';

        foreach ($args['fields'] as $fieldKey => $fieldVal) {
            $sql .= $fieldKey . ' =  :' . $fieldKey . ',';
        }

        $sql = rtrim($sql, ',');

        $sql .= ' WHERE id = ' . $args['id'];

        return $sql;
    }

	function parseWhereClause($where, $rel = 'AND')
	{
		$rel = isset($where['relation']) ? $where['relation'] : $rel;
        unset($where['relation']);

        $whereSql = ' ( ';

        $clauseCnt = count($where);
        $i = 1;

		foreach ($where as $key => $where2) {

			$field = isset($where2['field']) ? $where2['field'] : false;
			$compare = isset($where2['compare']) ? $where2['compare'] : false;
			$value = isset($where2['value']) ? $where2['value'] : '';

			unset($where2['field']);
			unset($where2['type']);
			unset($where2['compare']);
			unset($where2['value']);

            $isQueryClause = $field && $compare; // is it clause that contains query parameters (opposed to container clause which holds clauses)
            $isLastClause = $clauseCnt == $i;
            $hasChildClauses = !empty($where2) && is_array($where2);

            if ($isQueryClause) {
                // TODO: format value by data type
                $whereSql .= $field . ' ' . $compare . ' ' . '"' . $value . '"';
            }

            if ( !$isLastClause || $isLastClause && $isQueryClause && $hasChildClauses ) {
                $whereSql .= ' ' . $rel . ' ';
            }

			if ( $hasChildClauses ) {
				$whereSql .= $this->parseWhereClause($where2, $rel);
			}

            $i++;
		}

		$whereSql .= ' ) ';

		return $whereSql;

	}
}
