<?php
namespace App\Core;


class Validator
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILED = 'failed';
    const CONTEXT_MESSAGES = 'messages';
    const CONTEXT_ERRORS = 'errors';

    protected $rules;

    protected $messages = [
        'required' => 'Required field',
        'min' => 'Must be atleast %d characters long',
        'max' => 'Cannot exceed %d characters',
        'matches' => 'Must match %s',
    ];

    protected $status = [
        'status' => self::STATUS_FAILED,
        'msg' => 'Form has errors',
        'messages' => [],
        'errors' => [],
    ];

    function __construct($rules, $messages = [])
    {
        $this->rules = $rules;
        $this->messages = array_merge($this->messages, $messages);
    }


    function validate()
    {
        foreach ($this->rules as $rule) {

        }

        if ($this->hasErrors()) {
            $this->setStatus(self::STATUS_FAILED);
        } else {
            $this->setStatus(self::STATUS_SUCCESS);
        }
    }


    function hasPassed()
    {
        return $this->status['status'] == self::STATUS_SUCCESS;
    }


    function addError($key, $value)
    {
        $this->addMessage($key, $value, self::CONTEXT_ERRORS);
    }


    function getError($key)
    {
        return $this->getMessage($key, self::CONTEXT_ERRORS);
    }


    function addMessage($key, $value, $context = self::CONTEXT_MESSAGES)
    {
        $this->status[$context][$key] = $value;
    }


    function getMessage($key = null, $context = self::CONTEXT_MESSAGE)
    {
        if ($key == null) {
            return $this->status['msg'];
        } else {
            return isset($this->status[$context][$key]) ? $this->status[$context][$key] : null;
        }
    }


    function setStatus($status)
    {
        $this->status['status'] = $status;
    }


    function hasErrors()
    {
        return count($this->status[self::CONTEXT_ERRORS]) > 0;
    }
}