<?php
namespace App\Core;

class Helper
{
	
	public static function convertToStuldyCaps($string)
	{
		return str_replace(' ', '', ucwords(str_replace(' ', '-', $string)));
	}


	public static function convertToCamelCase($string)
	{
		return lcfirst(self::convertToStuldyCaps($string));
	}

	public static function url($path = '')
	{
		$request = Request::getInstance();
		echo $request->getBaseUrl($path);		
	}
}


