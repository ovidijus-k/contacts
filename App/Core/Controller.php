<?php
namespace App\Core;

use Exception;


abstract class Controller
{
	protected $routeParams;

	protected $request;

	function __construct($routeParams)
	{
		$this->routeParams = $routeParams;
		$this->request = Request::getInstance();
	}	

	function __call($name, $args)
	{

		$method = $name . 'Action';

		if (method_exists($this, $method)) {
			if ($this->before()) {
				call_user_func_array([$this, $method], $args);
				$this->after();

			}
		} else {
			throw new Exception("Method $method not found in controller " . get_class($this));
		}
	}


	protected function before()
	{
		return true;
	}


	protected function after()
	{

	}


	protected function getRequest()
	{
		return $this->request;
	}


	protected function hasParam($key)
	{
		return isset($this->routeParams[$key]);
	}


	protected function getParam($key, $default = false)
	{
		return isset($this->routeParams[$key]) ? $this->routeParams[$key] : $default;
	}

}