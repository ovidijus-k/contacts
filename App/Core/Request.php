<?php
namespace App\Core;


class Request extends Singleton
{

	private $url;
	private $baseUrl;
	private $path;
    private $baseUrlPrefix = '/public';
    private $query;

	function __construct()
	{
		$this->url = $_SERVER['REQUEST_URI'];
		$this->path = parse_url($this->url, PHP_URL_PATH);
		$this->baseUrlPrefix = Config::get('base-url-prefix');
		$this->query = $_REQUEST;

        if ($this->baseUrlPrefix) {
            $this->path = str_replace($this->baseUrlPrefix, '', $this->path);
        }
	}


	function getUrl($path = '')
	{
		return $this->url . $path;
	}


	function getBaseUrl($path = '')
	{
		return rtrim(Config::get('base-url') , '/') . '/' . $path;
	}


	function getPath()
	{
		return $this->path;
	}


	function has($key)
	{
		return isset($this->query[$key]);
	}


	function getAll()
	{
		return $this->query;
	}

	function get($key, $default = null)
	{
		return isset($this->query[$key]) ? $this->query[$key] : $default;
	}

	function getMethod()
	{
		return $_SERVER['REQUEST_METHOD'];
	}

}
