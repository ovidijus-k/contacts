<?php
namespace App\Core;


class App
{

	function __construct()
	{
        session_start();
		$this->initRoutes();
	}


	function initRoutes()
	{
		$routes = Config::get('routes');

		$router = Router::getInstance();

		foreach ($routes as $route) {
			$router->add($route);
		}
	}


	function run()
	{
		$router = Router::getInstance();
		$router->dispatch();
	}
}
