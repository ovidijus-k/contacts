<?php
namespace App\Core;

use Exception;


class Router extends Singleton
{
	private $params;
	private $routes;
	private $url;
	private $request;


	function __construct()
	{
		$this->request = Request::getInstance();
	}


	public function add($route)
	{
		if (is_array($route)) {
			$pattern = $route['pattern'];
			$method = !empty($route['method']) ? $route['method'] : 'GET';
			$params = $route['params'];
		} else {
			$pattern = $route;
			$method = 'GET';
			$params = [];
		}

		// Convert pattern to regular expression

		// Escape forward slashes for regular expression

		$pattern = preg_replace('/\//', '\\/', $pattern);


		/* 	Parse pattern variables into regular expressions.
			E.g: {controller} becomes (?P<controller>[a-z-]+). */

		$pattern = preg_replace('/\{([a-z]+)}/', '(?P<\1>[a-z-]+)', $pattern);

		/* 	Parse custom variables intro regular expressions.
			E.g.: {id:\d+} becomes (?P<id>\d+) */

		$pattern = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $pattern);


		$pattern = '/^\/' . $pattern . '$/i';

		$params['method'] = strtoupper($method);

		$this->routes[$pattern] = $params;
	}


	private function match($url)
	{
		// Retrieve controller, action params from regular expression pattern
		$matches = [];
		$params = [];

		foreach ($this->routes as $route => $params) {
			if ( $params['method'] == $this->request->getMethod()
				&& preg_match($route, $url, $matches) ) {
				foreach ($matches as $key => $match) {
					if (is_string($key)) {
						$params[$key] = $match;
					}
				}

				$this->params = $params;
				return true;
			}

		}

		return false;
	}


	public function dispatch()
	{

		$url = $this->request->getPath();

		if ($this->match($url)) {
			$controllerClass = Helper::convertToStuldyCaps($this->params['controller']);
			$controllerClass = 'App\Controllers\\' . $controllerClass . 'Controller';

			if ( class_exists($controllerClass) ) {
				$controllerObj = new $controllerClass($this->params);
				$actionMethod = Helper::convertToCamelCase($this->params['action']);

				if ( is_callable([$controllerObj, $actionMethod]) ) {
					$controllerObj->$actionMethod();
				} else {
					throw new Exception("Controller $controllerClass method $actionMethod not found or not callable (url: $url)");
				}
			} else {
				throw new Exception("Controller $controllerClass doesn't exist (url: $url)");
			}
		} else {
			throw new Exception("Failed to match route for url $url");
		}
	}


	public function getParams()
	{
		return $this->params;
	}


	public function getParam($key, $default = false)
	{
		return !empty($this->params[$key]) ? $this->params[$key] : $default;
	}
}
