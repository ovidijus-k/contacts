<?php
namespace App\Core;


class Paginator
{
    protected $model;
    protected $params = [];

    function __construct($model, $params = [])
    {
        $this->model = $model;

        $this->params = array_merge($this->params, $params);
    }


    public function get($page)
    {
        $this->query['page'] = $page;

        return $this->model->query($this->query);
    }


    public function getPages()
    {
        $request = Request::getInstance();
        $router = Router::getInstance();
        $pattern = '/page\/([0-9]+)/';

        if (!empty($this->params['baseUri'])) {
            $url = $request->getBaseUrl($this->params['baseUri']);    
            
        } else {
            $url = $request->getUrl();    
        }

        if ( preg_match($pattern, $url) === 0 ) {
            $url = rtrim($url, '/') . '/page/0';
        }
        

        $links = [];

        $pageCnt = $this->model->getTotalPages();
        
        $currentPage = $router->getParam('page', 1);


        // Add search params
        $search = $request->get('search');
        if (is_array($search) && !empty($search)) {
            $search = [
                'search' => $search,
            ];
            $url .= '?' . http_build_query($search);
        }

        for ( $i = 1; $i <= $pageCnt; $i++ ) {
            $links[$i] = [
                'page' => $i,
                'isActive' => $currentPage == $i,
                'link' => preg_replace($pattern, 'page/' . $i, $url),
            ];
        }

        return $links;
    }
}