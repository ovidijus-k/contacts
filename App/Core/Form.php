<?php
namespace App\Core;

abstract class Form
{
	protected $formId;

    protected $validator;

    protected $rules;


 	function __construct($formId, $rules)
    {
        $this->formId = $formId;
        $this->rules = $rules;
        $this->validator = new FormValidator($rules);

        $this->init();
    }

    private function init()
    {

    }

    public function isSubmitted()
    {
        return isset($_POST[$this->formId]) ?  true : false;
    }

    protected function verifyToken()
    {
        $token = Form::getToken();

        return $token == $_POST['token'];
    }


    protected function sanitizeData($data)
    {
        return $data;
    }


    protected function afterValidate()
    {
        return $this->validator->hasPassed();
    }


    protected function saveData()
    {
        return true;
    }


    public function getValidator()
    {
        return $this->validator;
    }


    protected function afterSubmit()
    {
    }


 	public function process()
    {

        // Maybe form was submitted

        if ($this->isSubmitted()) {

            if ( $this->verifyToken() ) {
                $this->request = $this->sanitizeData($_REQUEST);
                $this->validator->validate($this->request);

                if ($this->validator->hasPassed() && $this->afterValidate()) {
                    if ($this->saveData())
                        $this->afterSubmit();
                }

            } else {
                $this->validator->setStatus('failed', 'Token mismatch');
            }
        }
    }

    static function getToken()
    {
        if (empty($_SESSION['token'])) {
            $_SESSION['token'] = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 32);
        }

        return $_SESSION['token'];
    }

}
