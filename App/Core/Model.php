<?php
namespace App\Core;

use PDO;
use App\Core\Config;


abstract class Model
{

	protected $table;

	private static $db = null;


	protected $query = [
		'args' => [
			'pagesize' => 25,
		],
	];


	protected function getDB()
	{

		if (self::$db === null) {
			$host 		= Config::get('db.host');
			$dbname 	= Config::get('db.dbname');
			$username 	= Config::get('db.username');
			$password 	= Config::get('db.password');

			$options = [
			    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			    PDO::ATTR_EMULATE_PREPARES   => false,
			];

			try {
				self::$db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password, $options);
				return self::$db;
			} catch(\PDOException $e) {
				throw new \PDOException($e->getMessage(), (int)$e->getCode());
			}

		}

		return self::$db;
	}


	public function setArgs($args)
	{
		$this->query['args'] = array_merge($this->query['args'], $args);
	}


	private function clearQuery()
	{
		unset($this->query['found']);
		unset($this->query['total']);
		unset($this->query['totalpages']);
		unset($this->query['results']);
	}


    public function query()
    {
    	$this->clearQuery();

    	$args = $this->query['args'];
    	$db = self::getDB();

    	if ( ! $this->table ) {
    		throw new \Exception("Database table not not for model " . get_class());
    		return false;
    	}

        $queryParser = new SQLQueryParser($this->table);

        $parsed = $queryParser->parseQuery($args);

        $query = $db->prepare($parsed['sql']);
        $query->execute($parsed['params']);

        $this->query['results'] = $query->fetchAll();
        $this->query['found'] = count($this->query['results']);

        if ( ! isset($args['page']) ) {
        	$this->query['total'] =  $this->query['found'];
        	$this->query['totalpages'] =  1;
        }

        return $this->query['results'];
    }

    public function getTotal()
    {
    	if (isset($this->query['total']))
    		return $this->query['total'];

    	$db = self::getDB();
    	$args = $this->query['args'];
    	$pageSize = isset($args['pagesize']) ? $args['pagesize'] : 0;
    	if ($pageSize = 0)
    		return 0;

    	unset($args['page']);
    	unset($args['pagesize']);

        $queryParser = new SQLQueryParser($this->table);

        $parsed = $queryParser->parseQuery($args, 'count');

        $query = $db->prepare($parsed['sql']);
        $query->execute($parsed['params']);

        $total = $query->fetchColumn();

        if ($total == 0) {

        	$this->query['totalpages'] = 0;
        	return 0;
        }

        $this->query['total'] = $total;

		return $total;

    }

    public function getTotalPages()
    {
    	if (isset($this->query['totalpages']))
    		return $this->query['totalpages'];

    	$pageSize = isset($this->query['args']['pagesize']) ? $this->query['args']['pagesize'] : 0;

    	if ($pageSize == 0) {
        	$this->query['totalpages'] = 1;
        	return 1;
    	}

  		$total = $this->getTotal();

        if ($total == 0) {
        	$this->query['totalpages'] = 0;
        	return 0;
        }

        $totalPages = intval( ceil($total / $pageSize) );

		$this->query['totalpages'] = $totalPages;
		$this->query['total'] = $total;

		return $this->query['totalpages'];

    }


    public function exists($field, $value)
    {

		$args = [
			'pagesize' => 1,
			'where' => [
				[
					'field' => $field,
					'compare' => '=',
					'value' => $value,
				],
			]
		];

		$this->setArgs($args);

		$this->query();

    	return $this->getTotal() > 0;
    }

	public function get($id)
	{
        $db = self::getDB();
        $query = $db->prepare("SELECT * FROM " . $this->table . " WHERE id = :id LIMIT 1");
        $query->execute(['id' => $id]);

        return $query->fetch();

	}


	public function create(array $fields)
	{

		$db = self::getDB();
		$queryParser = new SQLQueryParser($this->table);

		$params = [
			'fields' => $fields,
		];

		$sql = $queryParser->parseInsertQuery($params);

		$query = $db->prepare($sql);
		$query->execute($fields);
	}


	public function update($id, array $fields)
	{
        $db = self::getDB();
		$queryParser = new SQLQueryParser($this->table);

        $params = [
            'id' => $id,
            'fields' => $fields,
        ];

        $sql = $queryParser->parseUpdateQuery($params);

        $query = $db->prepare($sql);
		$query->execute($fields);
	}

    public function delete($id)
    {
        $db = self::getDB();

        if (is_numeric($id)) {
            $sql = "DELETE FROM " . $this->table . " WHERE id = :id ";
            $query = $db->prepare($sql);
            $query->execute(['id' => $id]);
        } elseif (strpos($id, ',') !== FALSE && !empty(explode(',', $id))) {
            $sql = "DELETE FROM " . $this->table . " WHERE id IN($id)";
            $query = $db->prepare($sql);
            $query->execute();
        } else {
            return false;
        }



        return $query->rowCount() > 0;
    }
}
