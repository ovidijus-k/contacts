<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Contacts</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="manifest" href="site.webmanifest">

  <link rel="stylesheet" href="<?php self::url('assets/css/main.css') ?>">
  <link rel="stylesheet" href="<?php self::url('assets/lib/bootstrap/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
    <div class="page">
        <div class="page-header clearfix">
          <div class="container">
            <div class="page-header__title">
              <h1><a href="<?php self::url('') ?>">CONTACTS</a></h1>
            </div>
            <div class="page-header__menu">
              <ul class="menu">
                <!-- <li><a href="<?php self::url('') ?>">Index</a></li> -->
                <li><a href="<?php self::url('contacts/create') ?>">New Contact</a></li>
              </ul>
            </div>
          </div>

        </div>
        <div class="container page-container">
