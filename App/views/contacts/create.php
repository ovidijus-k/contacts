<div class="row">
	<div class="col-md-6 mx-auto">
		<div class="card card-body bg-light mt-5">
			<h2>New contact</h2>

			<?php if (!empty($statusMessage)) : ?>
				<div class="alert alert-<?php echo $statusCode == 'passed' ? 'success' : 'danger' ?>" role="alert">
	  				<?php echo $statusMessage ?>
				</div>
			<?php endif; ?>

			<form action="<?php echo self::currentUrl() ?>" method="post">
                <input type="hidden" name="token" value="<?php echo self::token() ?>">
				<div class="form-group">
					<label for="fullname">Fullname <sup>*</sup></label>
					<input type="text" name="fullname" class="form-control form-control-lg <?php echo !empty($errors['fullname']) ? 'is-invalid' : '' ?>" value="<?php echo self::input('fullname') ?>">
					<?php if (!empty($errors['fullname'])) : ?>
						<span class="invalid-feedback"><?php echo $errors['fullname'] ?></span>
					<?php endif; ?>
				</div>
				<div class="form-group">
					<label for="phone">Phone <sup>*</sup></label>
					<input type="text" name="phone" class="form-control form-control-lg <?php echo !empty($errors['phone']) ? 'is-invalid' : '' ?>" value="<?php echo self::input('phone') ?>">
					<?php if (!empty($errors['phone'])) : ?>
						<span class="invalid-feedback"><?php echo $errors['phone'] ?></span>
					<?php endif; ?>
				</div>
				<div class="row">
					<div class="col">
						<input name="contacts_create" type="submit" value="Create" class="btn btn-success btn-block">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
