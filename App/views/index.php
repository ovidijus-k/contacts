<?php 
	$searchInput = self::input('search');
?>
<form action="<?php echo self::url('contacts/page/1') ?>" method="get">
	<table class="table">
		<thead>
			<tr>
				<th>
				    <input data-action-toggle-check-all title="Check/uncheck all" type="checkbox" id="check-all">
				</th>
				<th>Fullname</th>
				<th>Phone</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td></td>
				<td><input class="form-control" placeholder="Search by fullname" type="text" name="search[fullname]" value="<?php echo!empty($searchInput['fullname']) ? $searchInput['fullname'] : ''  ?>"></td>
				<td><input class="form-control" placeholder="Search by phone" type="text" name="search[phone]" value="<?php echo!empty($searchInput['phone']) ? $searchInput['phone'] : ''  ?>"></td>
				<td>
					<button class="btn btn-link"><i class="material-icons">search</i></button>
					<button data-action-delete-checked class="btn btn-link"><i class="material-icons">remove_circle</i></button>
				</td>
			</tr>
			<?php foreach ($contacts as $contact) : ?>
				<tr>
					<td><input name="entities[]" value="<?php echo $contact['id'] ?>" type="checkbox" id="check-all"></td>
					<td><?php echo $contact['fullname'] ?></td>
					<td><?php echo $contact['phone'] ?></td>
					<td>
	                    <a href="<?php echo self::url('contacts/' . $contact['id'] . '/edit') ?>" title="Edit" data-edit="<?php echo $contact['id'] ?>"><i class="material-icons">border_color</i></a>
	                    <a href="#" title="Delete" data-action-delete="<?php echo $contact['id'] ?>"><i class="material-icons">remove_circle</i></a>
	                </td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</form>
<?php if (count($pages) > 1) : ?>
	<ul class="pagination">
		<?php foreach ($pages as $pageNum => $link) : ?>
			<li class="page-item <?php echo $link['isActive'] ? 'active' : '' ?>">
				<a class="page-link"
				href="<?php echo $link['link'] ?>"><?php echo $pageNum ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
